const NextFederationPlugin = require("@module-federation/nextjs-mf");

const remotes = (isServer) => {
  const location = isServer ? "ssr" : "chunks";
  const homeUrl = process.env.HOME_URL || "http://localhost:3000";

  return {
    home: `home@${homeUrl}/_next/static/${location}/remoteEntry.js`,
  };
};

module.exports = {
  images: {
    domains: ["img.salehere.co.th"],
  },
  webpack(config, options) {
    config.plugins.push(
      new NextFederationPlugin({
        name: "article",
        filename: "static/chunks/remoteEntry.js",
        exposes: {
          "./title": "./src/components/exposedTitle.js",
          "./article": "./src/pages/article/index.js",
          "./article-slug": "./src/pages/article/[...slug].js",
          "./pages-map": "./pages-map.js",
        },
        remotes: remotes(options.isServer),
        shared: {},
        extraOptions: {
          exposePages: true,
        },
      })
    );

    return config;
  },
};
