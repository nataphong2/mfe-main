import { gql } from '@apollo/client';
import { SECTION } from '../fragments/section'

export const GET_ALL_ARTICLES = gql`
  query getArticles($offset: Int, $limit: Int, $isPublic: Boolean, $filter: ArticleFilter) {
    articles(offset: $offset, limit: $limit, isPublic: $isPublic, filter: $filter) {
      id
      title
      views
      createdDate {
        short
      }
      updatedDate {
        short
      }
      seoSlugUrl
      status {
        like {
          count
          isInteract
        }
        bookmark {
          isInteract
        }
      }
      thumbnailImg {
        imgLowPath
        imgSmallPath
        imgPath
        imgMediumPath
        imgAlt
        width
        height
      }
      author {
        name
        profileImage {
          imgThumbPath
          imgAlt
        }
      }
      statistic {
        numberOfComments
      }
    }
  }
`;

export const GET_ARTICLE_INFO = gql`
  ${SECTION}

  query getArticleInfo($slug: String!, $full: Boolean) {
    slug(slug: $slug, full: $full) {
      article {
        id
        title
        views
        shortDescription
        isObsolete
        content
        author {
          id
          name
          userSlug
          profileImage {
            imgThumbPath
            imgAlt
          }
        }
        sections {
          ...SectionContent
        }
        thumbnailImg {
          imgPath
          imgAlt
        }
        coverImages {
          id
          imgAlt
          imgSmallPath
          imgMediumPath
          imgPath
        }
        advertises {
          id
          title
          link
          weight
          coverImg {
            imgPath
            imgLowPath
            imgAlt
            imgMediumPath
            imgSmallPath
          }
        }
        seoTagTitle
        seoTagDescription
        seoSlugUrl
        like
        views
        status {
          like {
            count
            isInteract
          }
          bookmark {
            isInteract
          }
        }
        category {
          id
          nameTh
          nameEn
          slug
        }
        categories {
          id
          name
          slug
        }
        images {
          imgAlt
          imgPath
        }
        tags {
          id
          name
          slugUrl
        }
        updatedAt
        createdAt
        updatedDate {
          full
          short
        }
        createdDate {
          full
        }

        statistic {
          numberOfComments
          numberOfReplyComments
          numberOfShares
        }
      }
    }
  }
`;
