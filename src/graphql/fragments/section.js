import { gql } from "@apollo/client";

export const SECTION = gql`
  fragment SectionContent on Section {
    __typename
    ... on EntitySectionText {
      value
      textType: type
    }
    ... on EntitySectionList {
      listType: type
      values
    }
    ... on Image {
      imgLowPath
      imgPath
      imgAlt
      imgFigure
      width
    }
    ... on ButtonSection {
      image {
        imgLowPath
        imgPath
        imgAlt
        width
      }
      link
    }
    ... on BannerSectionList {
      banners {
        image {
          imgPath
        }
        link
      }
    }
    ... on ShoppingProductSectionList {
      title
      products {
        id
        seoSlugUrl
        name
        price
        amount
        reviews {
          rating {
            averageRating
          }
        }
        discountPrice
        properties {
          amount
          label
          id
          price
          discountPrice
        }
        images {
          imgPath
          id
        }
        brand {
          seoSlugUrl
        }
        totalSold
        purchaseLimit
        deletedDate {
          shortDateTimeSec
        }
      }
    }
  }
`;
