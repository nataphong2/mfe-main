import { ApolloClient, InMemoryCache } from "@apollo/client";

const createApolloClient = () => {
  return new ApolloClient({
    uri: "https://sit-api.sh-pxmer.co/graphql",
    cache: new InMemoryCache(),
  });
};

export default createApolloClient;
