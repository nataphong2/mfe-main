import App from "next/app";
import { ApolloProvider } from "@apollo/client";
import createApolloClient from "@/apollo-client";
import "@/css/global.css";

function MyApp({ Component, pageProps }) {
  const client = createApolloClient();

  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

MyApp.getInitialProps = async (ctx) => {
  const appProps = await App.getInitialProps(ctx);
  return appProps;
};

export default MyApp;
