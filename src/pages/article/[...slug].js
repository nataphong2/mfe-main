import Image from "next/image";
import { GET_ARTICLE_INFO } from "@/graphql/queries/article";
import createApolloClient from "@/apollo-client";

export const getServerSideProps = async ({ params }) => {
  const client = createApolloClient();
  const [slug] = params.slug;
  const { data } = await client.query({
    query: GET_ARTICLE_INFO,
    variables: {
      slug,
      full: false,
    },
  });

  return {
    props: {
      data: data.slug.article,
    },
  };
};

export default function ArticleById({ data }) {
  return (
    <div
      style={{
        display: "flex",
        padding: "20px",
        flexDirection: "column",
      }}
    >
      <div>
        <Image
          style={{
            width: "100%",
          }}
          src={data.thumbnailImg.imgPath}
          width={200}
          height={200}
        />
      </div>
      <h1
        style={{
          fontWeight: "bold",
          fontSize: "40px",
        }}
      >
        {data.title}
      </h1>
    </div>
  );
}
