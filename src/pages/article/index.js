import React, { lazy } from "react";
import Head from "next/head";
import Image from "next/image";
import { GET_ALL_ARTICLES } from "@/graphql/queries/article";
import createApolloClient from "@/apollo-client";

export const getServerSideProps = async () => {
  const client = createApolloClient();
  const { data } = await client.query({
    query: GET_ALL_ARTICLES,
    variables: {
      limit: 12,
      offset: 0,
    },
  });

  return {
    props: {
      data: data.articles,
    },
  };
};

const Article = ({ data = [] }) => {
  return (
    <>
      <Head>
        <title>Article</title>
        <link rel="icon" href="/nextjs-ssr/article/public/favicon.ico" />
      </Head>

      <div
        style={{
          display: "flex",
          padding: "20px",
        }}
      >
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr 1fr 1fr",
            gap: "12px",
          }}
        >
          {data.map((item) => (
            <a
              key={item.id}
              href={`/article/${item.seoSlugUrl}`}
              style={{
                border: "1px solid pink",
                borderRadius: "10px",
                overflow: "hidden",
                cursor: "pointer",
              }}
            >
              <Image
                style={{
                  width: "100%",
                }}
                src={item.thumbnailImg.imgPath}
                width={item.thumbnailImg.width}
                height={200}
              />
              <div
                style={{
                  padding: "8px",
                }}
              >
                <p
                  style={{
                    fontWeight: "bold",
                  }}
                >
                  {item.title}
                </p>
              </div>
            </a>
          ))}
        </div>
      </div>
    </>
  );
};

export default Article;
