export default {
  '/article': './article',
  '/article/*': './article-slug'
};
